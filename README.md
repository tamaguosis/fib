# README #

How to run application: VS -> CTRL + F5 

# SHOULD or SHOULD NOT #

Firstly, I would rewrite method which finds next Fib number because right now it does not work right if the first number is 0.
Secondly, I'm not sure if semaphore is a right thing to use for this solution, so I would check for other solutions if more time would be given.
Thirdly, I would check how to share data between Threads in a proper way, because right now I'm not confident with my solution