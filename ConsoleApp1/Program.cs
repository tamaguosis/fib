using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace ConsoleApp1
{
    public class SharedData
    {
        public SharedData()
        {
            InitValues = new List<long>();
        }

        public long? Last { get; set; }

        public List<long> InitValues { get; set; }
    }

    class Program
    {
        static Thread[] threads = new Thread[3];
        static Semaphore semaphore = new Semaphore(1, 1);

        static void FibWorker(SharedData data, long number)
        {
            var closest = GetClosestFibNumber(number);

            lock (data)
            {
                data.InitValues.Add(closest);
            }

            CalcFib(data, data.InitValues.Min());
        }

        private static void CalcFib(SharedData data, long n)
        {
            semaphore.WaitOne();

            var number = NextNumber(data.Last ?? data.InitValues.Min());

            if (data.InitValues.Max() > number)
            {
                Console.WriteLine("{0}: {1}", Thread.CurrentThread.Name, number);
                data.Last = number;

                semaphore.Release();
                CalcFib(data, number);
            }
        }

        static long NextNumber(long n)
        {
            double a = n * (1 + Math.Sqrt(5)) / 2.0;
            return (long)Math.Round(a);
        }

        private static long GetClosestFibNumber(long n)
        {
            int a = 0, b = 1;

            for (; b < n; a = b - a)
            {
                b += a;
            }

            var closest = n - a > b - n ? b : a;

            return closest;
        }

        static void Main(string[] args)
        {
            var data = new SharedData();
            var initValues = new List<long> { 1, 13, 6765 };

            for (int i = 0; i < 3; i++)
            {
                var value = initValues[i];
                threads[i] = new Thread(() => FibWorker(data, value)) { Name = "node-" + i };
                threads[i].Start();
            }
            Console.Read();

            Console.WriteLine("[{0}:{1}]", data.InitValues.Min(), data.InitValues.Max());
        }
    }
}
